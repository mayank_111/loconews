# loconews

## Overview

A django project to fetch location and news.

## Setup

Create a virtual environment in which to install Python pip packages. Virtual environment activation with [virtualenv](https://pypi.python.org/pypi/virtualenv),

    python3 -m virtualenv venv          # create virtualenv venv
    source venv/bin/activate # activate 

Install development dependencies,

    pip3 install -r requirements.txt

Setup database tables,

    python manage.py migrate  # create neccessary tables

Run the web application locally,

    python manage.py runserver # 127.0.0.1:8000
