from django.urls import re_path
from home import views

urlpatterns = [
    re_path(r'^$', views.home, name='home_view'),
]
