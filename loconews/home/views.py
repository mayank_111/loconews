from django.shortcuts import render


def home(request):
    """ This is the home view and displays a search box """

    return render(request, "home.html")
