from django.contrib import admin
from search.models import Location, News

admin.site.register(Location)
admin.site.register(News)
