from django.db import models


class Location(models.Model):
    """ This is a model class for location """
    name = models.CharField(max_length=200)
    refreshed_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}".format(self.name)


class News(models.Model):
    """ This is a model class for news which represents one news
        related to a location """
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    url = models.URLField()
    urlToImage = models.URLField(null=True)
    source = models.CharField(max_length=200)
    publishedAt = models.DateTimeField()

    def __str__(self):
        return "{} - {}".format(self.location.name, self.title)
