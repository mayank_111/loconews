from django.shortcuts import render, redirect
from django.utils import timezone

import datetime

from newsapi import NewsApiClient   # Will be used for newsapi

from search.models import Location, News
from loconews.settings import GOOGLE_API_KEY, GOOGLE_MAPS_URL, NEWS_API_KEY



def check_time(time, period=12):
    """ This function takes a DateTimeObject and time_period as the argument
        and returns True if the currentTime-time < 6 else False """

    timedelta = timezone.now() - time
    total_secs = timedelta.total_seconds()
    hours = divmod(total_secs, 3600)[0]
    if hours > period:
        return True
    return False



def search(request):
    """ This is the search method which makes the api calls
        and stores the result on the database """

    # Redirect if the HTTP method is GET
    if request.method == "GET":
        return redirect('/')


    if request.method == 'POST':
        # For uniformity in the database storing location names in uppercase
        query = request.POST.get('place').upper()

        # Return to home page if query is an empty string
        if not query:
            return redirect('/')

        # Check if the query is already in the database
        try:
            location = Location.objects.get(name=query)
        except:
            location = None

        # If the location is not present in the database or the refreshed_at
        # time is more than 12hr then make an API call to newsapi
        if location is None:

            # Creating a new location object
            location = Location.objects.create(name=query)

            newsapi = NewsApiClient(api_key=NEWS_API_KEY)
            all_articles = newsapi.get_everything(q=query,
                                          language='en',
                                          sort_by='publishedAt',
                                          page=2)
            # Only if the news are found
            if all_articles:
                for article in all_articles['articles']:
                    news = News()
                    news.location = location
                    news.title = article['title']
                    news.url = article['url']
                    news.urlToImage = article['urlToImage']
                    news.source = article['source']['name']
                    news.publishedAt = article['publishedAt']
                    news.save()

        elif location and check_time(location.refreshed_at):

            # Deleting the existing News model objects for the given location
            News.objects.filter(location=location).delete()

            # Setting new refreshed_at time for location
            location.refreshed_at = timezone.now()
            location.save()

            # Fetching latest News and creating new models
            newsapi = NewsApiClient(api_key=NEWS_API_KEY)

            all_articles = newsapi.get_everything(q=query,
                                          language='en',
                                          sort_by='publishedAt',
                                          page=2)

            if all_articles:
                for article in all_articles['articles']:
                    news = News()
                    news.location = location
                    news.title = article['title']
                    news.url = article['url']
                    news.urlToImage = article['urlToImage']
                    news.source = article['source']['name']
                    news.publishedAt = article['publishedAt']
                    news.save()


        # Will return empty queryset if no news is found for the location
        # Will return a queryset containing news about the location if found
        top_headlines = News.objects.filter(location=location)

        # Making the API call to fetch the location on Map
        query_optimized = query.replace(' ', '+')
        response_map = GOOGLE_MAPS_URL.format(GOOGLE_API_KEY, query_optimized)

        return render(request, "search.html",
                    {"response": response_map, "query": query,
                    "top_headlines": top_headlines})
